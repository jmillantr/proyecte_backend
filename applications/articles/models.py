from __future__ import unicode_literals

from random import randint

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from applications.utils.validators import valid_media_file

# TODO: Mirar de incluir Tags, en Articulo o Post


class Article(models.Model):
    owner = models.ForeignKey(User)
    created_date = models.DateTimeField(auto_now_add=True)  # Fecha al crear elemento
    updated_date = models.DateTimeField(auto_now=True)  # Fecha al hacer un .save()
    num_posts = models.IntegerField(default=0)
    random_order = models.IntegerField(default=0)

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"
        ordering = ("-created_date",)

    def update_random_order(self):
        """
        Actualiza el numero de orden aleatorio actual
        """
        self.random_order = randint(0, Article.objects.count())


class Post(models.Model):
    owner = models.ForeignKey(User)
    created_date = models.DateTimeField(auto_now_add=True)  # auto_now_add, ademas nos permite modificarlo
    article = models.ForeignKey(Article, related_name='posts', on_delete=models.CASCADE)
    media = models.FileField(upload_to='articles/', validators=[valid_media_file])
    caption = models.CharField(max_length=255, null=True, blank=True, default=None)

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"
        ordering = ("-created_date",)


@receiver(post_save, sender=Post)
def create_profile(sender, instance=None, created=False, **kwargs):
    """
    Tras crear un nuevo Post
    - Recontar el numero de Post del Articulo
    - Marcar la ultima fecha de modificacion
    """
    if created:
        instance.article.num_posts = instance.article.posts.count()
        instance.article.save()


@receiver(post_save, sender=Article)
def set_random_order(sender, instance=None, created=False, **kwargs):
    """
    Tras crear un nuevo Articulo
    - Actualiza su numero de orden aleatorio
    """
    if created:
        instance.update_random_order()
        instance.save()
