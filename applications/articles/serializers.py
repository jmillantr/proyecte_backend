from rest_framework.fields import SerializerMethodField, CurrentUserDefault
from rest_framework.reverse import reverse
from rest_framework import serializers

from applications.articles.models import Article, Post
from applications.authentication.serializers import UserSerializer


class PostSerializer(serializers.ModelSerializer):
    """
    Post detallado
    """
    owner = serializers.StringRelatedField()

    class Meta:
        model = Post
        fields = ('pk', 'owner', 'created_date', 'media', 'caption')


class ArticleSerializer(serializers.ModelSerializer):  # ModelSerializer, no genera una URL
    """
    Articulo detallado
    """
    owner = serializers.StringRelatedField()
    posts = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = ('owner', 'created_date', 'posts')

    def get_posts(self, article):
        return reverse('api:article-post-list-create', args=(article.pk,), request=self.context['request'])


class ArticleSummarySerializer(serializers.ModelSerializer):
    """
    Articulo resumido, con la ultima entrada y las relacionadas
    """
    related = SerializerMethodField()  # Llama directamente a 'get_related'
    last = SerializerMethodField()  # Llama directamente a 'get_last'
    detail = serializers.SerializerMethodField()
    posts = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = ('pk', 'updated_date', 'detail', 'posts', 'last', 'related')

    def get_related(self, article):
        posts = Post.objects.filter(article=article)[1:4]
        serializer = PostSerializer(instance=posts, many=True)
        return serializer.data

    def get_last(self, article):
        post = article.posts.first()
        # post = Post.objects.filter(article=article)[:1]
        serializer = PostSerializer(instance=post)
        return serializer.data

    def get_detail(self, article):
        return reverse('api:article-detail', args=(article.pk,), request=self.context['request'])

    def get_posts(self, article):
        return reverse('api:article-post-list-create', args=(article.pk,), request=self.context['request'])

