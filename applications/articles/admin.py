from django.contrib import admin

from .models import Article, Post

admin.site.register(Article)
admin.site.register(Post)
