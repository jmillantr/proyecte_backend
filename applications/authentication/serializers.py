from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password as auth_validate_password

from .models import Profile
from rest_framework import serializers


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ('avatar',)


class ProfileDetailSerializer(ProfileSerializer):

    class Meta:
        model = Profile
        fields = ('avatar', 'facebook', 'twitter', 'pinterest', 'dribbble')


class UserSerializer(serializers.ModelSerializer):
    """
    Serializador de usuario generico
    """
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True, write_only=True, style={'input_type': 'password'})
    confirm_password = serializers.CharField(required=True, write_only=True, style={'input_type': 'password'})
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'confirm_password', 'profile')
        write_only_fields = ('username',)

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        validated_data.pop('confirm_password')  # Quitamos el campo extra
        user = User.objects.create(**validated_data)
        user.set_password(validated_data.get('password'))
        user.profile.avatar = profile_data.get('avatar')
        user.save()
        user.profile.save()
        return user

    def validate_password(self, value):
        auth_validate_password(value)
        return value

    def validate_email(self, email):
        existing = User.objects.filter(email=email).first()
        if existing:
            raise serializers.ValidationError(
                "Someone with that email address has already registered. Was it you?")

        return email

    def validate(self, data):
        if not data.get('password') or not data.get('confirm_password'):
            raise serializers.ValidationError(
                "Please enter a password and confirm it.")

        if data.get('password') != data.get('confirm_password'):
            raise serializers.ValidationError("Those passwords don't match.")

        return data


class UserDetailSerializer(UserSerializer):
    profile = ProfileDetailSerializer()
    post_count = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('username', 'date_joined', 'email', 'profile', 'post_count')
        write_only_fields = ('username',)

    def get_post_count(self, obj):
        return obj.post_set.count()


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_new_password(self, value):
        auth_validate_password(value)
        return value


class ChangeSocialSerializer(serializers.Serializer):
    """
    Serializer for social networks change endpoint.
    """
    facebook = serializers.URLField(required=False)
    twitter = serializers.URLField(required=False)
    pinterest = serializers.URLField(required=False)
    dribbble = serializers.URLField(required=False)

    def update(self, instance, validated_data):
        instance.facebook = validated_data.get('facebook', instance.facebook)
        instance.twitter = validated_data.get('twitter', instance.twitter)
        instance.pinterest = validated_data.get('pinterest', instance.pinterest)
        instance.dribbble = validated_data.get('dribbble', instance.dribbble)
        instance.save()
        return instance
