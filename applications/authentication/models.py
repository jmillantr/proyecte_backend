from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from applications.utils.validators import valid_media_file


@python_2_unicode_compatible
class Profile(models.Model):
    owner = models.OneToOneField(User)
    # TODO: Comprobar si hace falta cambiar el Storage, para que se eliminen los avatares antiguos
    avatar = models.FileField(upload_to='profiles/', validators=[valid_media_file],
                              null=True, blank=True, default=None)
    # TODO: Agregar following
    facebook = models.URLField(max_length=255, null=True, blank=True, default=None)
    twitter = models.URLField(max_length=255, null=True, blank=True, default=None)
    pinterest = models.URLField(max_length=255, null=True, blank=True, default=None)
    dribbble = models.URLField(max_length=255, null=True, blank=True, default=None)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_profile(sender, instance=None, created=False, **kwargs):
    """
    Asignar un Perfil y un Token automaticamente, tras crear un nuevo usuario
    """
    if created:
        Profile.objects.create(owner=instance)
