from django.conf.urls import url
from applications.rss.views import LatestEntriesFeed

urlpatterns = [
    url(r'^$', LatestEntriesFeed()),
]
