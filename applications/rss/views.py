from django.contrib.syndication.views import Feed
from applications.articles.models import Post


class LatestEntriesFeed(Feed):
    """
    Vista rss+xml para el canal RSS de los ultimos posts agregados
    """
    title = "Last Posts Created"
    link = "https://memestring.com"
    description = "Last added posts information."

    def items(self):
        return Post.objects.order_by('-created_date')[:50]

    def item_title(self, item):
        return 'Created by {0} on {1}'.format(item.owner.username,
                                              item.created_date)

    def item_description(self, item):
        return item.caption

    def item_link(self, item):
        return '{link}/#!/posts/{article}?view={post}'.format(link=self.link,
                                                              article=item.article.pk,
                                                              post=item.pk)
