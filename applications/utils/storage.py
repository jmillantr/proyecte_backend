# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import os


class OverwriteStorage(FileSystemStorage):
    """
    Sobreescribe el archivo si este nombre ya existe, incluso si la extension es ditinta
    Ej. avatar.png, sobrescribiria avatar.jpg, quedando solo avatar.png
    """
    # TODO: Hacer que sobrescriba tambien si la extension es distinta
    def get_available_name(self, filename, **kwargs):
        if self.exists(filename):
            os.remove(os.path.join(settings.MEDIA_ROOT, filename))
        return filename
