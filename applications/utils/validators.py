# -*- coding: utf-8 -*-

import os
from django.core.exceptions import ValidationError


def valid_media_file(value):
    ext = os.path.splitext(value.name)[1]  # [1] returns extenson
    valid_extensions = ['.jpg', '.png', '.gif']  # TODO: Permitir otro tipos
    if not ext.lower() in valid_extensions:
        raise ValidationError('Invalid extension')
