from django.contrib.auth.models import User
from rest_framework.compat import is_authenticated
from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsOwnerOrReadOnly(BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in SAFE_METHODS:
            return True

        # Comprobar si ya se trata del modelo usuario, que no puede tener 'owner'
        if isinstance(obj, User):
            return obj == request.user

        # Instance must have an attribute named `owner`.
        return obj.owner == request.user


class IsNotAuthenticatedToPost(BasePermission):
    """
    Permite solo realizar UNSAFE_METHODS si no se esta logeado,
    ideal para crear nuevos usuarios
    """

    def has_permission(self, request, view):
        return (
            request.method in SAFE_METHODS or
            request.user and
            not is_authenticated(request.user)
        )
