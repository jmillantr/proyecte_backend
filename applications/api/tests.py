from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework import status


class UserListCreateTest(APITestCase):
    def setUp(self):
        self.url_create = reverse('api:user-list-create')
        self.data = {
            "username": "test",
            "email": "test@test.com",
            "password": "Test2017!",
            "confirm_password": "Test2017!",
            "profile": {
                "avatar": None
            }
        }

    def test_create_user(self):
        """
        Ensure we can create a new user account.
        """
        print ('Crear nuevo usuario...')
        response = self.client.post(self.url_create, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.first().username, 'test')
        print ('OK')


class ArticleListCreateTest(APITestCase):
    def setUp(self):
        self.url_list = reverse('api:article-list-create')
        self.superuser = User.objects.create_superuser('john', 'john@snow.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')

    def test_list_article(self):
        """
        Ensure we can create a new article.
        """
        print ('Listar articulos...')
        response = self.client.get(self.url_list, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print ('OK')
