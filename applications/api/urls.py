from django.conf.urls import url
from applications.api import views
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token


"""
Users URLs
CRUD
"""
urlpatterns = [
    url(r'^users/$', views.UserListCreate.as_view(), name='user-list-create'),
    url(r'^users/(?P<username>\w+)/$', views.UserDetail.as_view(), name='user-detail'),
    url(r'^users/(?P<username>\w+)/change_profile/', views.ChangeProfile.as_view(), name='user-change_profile'),
    url(r'^users/(?P<username>\w+)/change_password/', views.ChangePassword.as_view(), name='user-change_password'),
    url(r'^users/(?P<username>\w+)/change_social/', views.ChangeSocial.as_view(), name='user-change_social'),
]


"""
Authentication Token
    Acces Using Token
    $ curl -H "Authorization: JWT <your_token>" http://localhost:8000/protected-url/
"""
urlpatterns += [
    # Get Token
    # $ curl -X POST -H "Content-Type: application/json" -d '{"username":"admin","password":"password123"}' http://localhost:8000/auth/
    url(r'^auth/', obtain_jwt_token),

    # Refresh Token
    # $ curl -X POST -H "Content-Type: application/json" -d '{"token":"<EXISTING_TOKEN>"}' http://localhost:8000/auth/refresh/
    url(r'^auth/refresh/', refresh_jwt_token),

    # Verify Token
    # $ curl -X POST -H "Content-Type: application/json" -d '{"token":"<EXISTING_TOKEN>"}' http://localhost:8000/auth/verify/
    url(r'^auth/verify/', verify_jwt_token),
]


"""
Articles URLs
"""
urlpatterns += [
    url(r'^articles/$', views.ArticleListCreate.as_view(), name='article-list-create'),
    url(r'^articles/(?P<pk>[0-9]+)/$', views.ArticleDetail.as_view(), name='article-detail'),
    url(r'^articles/(?P<article_pk>[0-9]+)/posts/$', views.ArticlePostListCreate.as_view(),
        name='article-post-list-create'),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json'])
