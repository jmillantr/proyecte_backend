from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.decorators import permission_classes
from rest_framework.generics import get_object_or_404, ListCreateAPIView, RetrieveAPIView, UpdateAPIView, RetrieveDestroyAPIView
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from applications.api.permissions import IsOwnerOrReadOnly, IsNotAuthenticatedToPost
from applications.articles.models import Article, Post
from applications.articles.serializers import ArticleSerializer, PostSerializer, ArticleSummarySerializer
from applications.authentication.models import Profile
from applications.authentication.serializers import UserSerializer, ChangePasswordSerializer, ProfileSerializer, \
    UserDetailSerializer, ChangeSocialSerializer


@permission_classes((IsAuthenticatedOrReadOnly,))
class ArticleListCreate(ListCreateAPIView):
    """
    Listar o Crear Articulos
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSummarySerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.parser_classes = (FormParser, MultiPartParser)
        serializer = PostSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            new_article = Article.objects.create(owner=request.user)
            serializer.save(owner=request.user, article=new_article)
            headers = self.get_success_headers(serializer.data)
            response = dict(serializer.data)
            response['article_pk'] = new_article.pk
            return Response(response, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        """
        Filtrar resultados segun el 'source' especificado por GET
        EJ: /?source=popular
        """
        queryset = Article.objects.all()
        source = self.request.query_params.get('source', 'popular')

        if source == 'popular':
            queryset.order_by('-num_posts')
        elif source == 'recent':
            queryset.order_by('-updated_date')
        elif source == 'random':
            queryset.order_by('random_order')

        return queryset


class ArticleDetail(RetrieveAPIView):
    """
    Detalla un Articulo
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


@permission_classes((IsAuthenticatedOrReadOnly,))
class ArticlePostListCreate(ListCreateAPIView):
    """
    Listar o Crear Post de un articulo especifico
    """
    serializer_class = PostSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        article = get_object_or_404(Article, pk=self.kwargs.get('article_pk'))
        self.parser_classes = (FormParser, MultiPartParser)
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid(raise_exception=True):
            serializer.save(owner=request.user,
                            article=article)
            headers = self.get_success_headers(serializer.data)
            response = dict(serializer.data)
            response['article_pk'] = article.pk
            return Response(response, status=status.HTTP_201_CREATED, headers=headers)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        article = get_object_or_404(Article, pk=self.kwargs.get('article_pk'))
        return article.posts.all()


@permission_classes((IsNotAuthenticatedToPost,))
class UserListCreate(ListCreateAPIView):
    """
    Listar o Crear Usuarios
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


@permission_classes((IsOwnerOrReadOnly,))
class UserDetail(RetrieveDestroyAPIView):
    """
    Visualizar o Actualizar si es el propietario
    """
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    lookup_field = 'username'

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


@permission_classes((IsOwnerOrReadOnly,))
class ChangePassword(UpdateAPIView):
    """
    Modifica la password
    {
        "old_password": "old_pass",
        "new_password": "new_pass"
    }
    """
    queryset = User.objects.all()
    serializer_class = ChangePasswordSerializer
    lookup_field = 'username'

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            update_session_auth_hash(request, self.object)
            return Response("Success.", status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsOwnerOrReadOnly,))
class ChangeProfile(UpdateAPIView):
    """
    Modificar imagen de perfil
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    lookup_field = 'username'

    def get_object(self):
        self.queryset = User.objects.all()
        user = super(ChangeProfile, self).get_object()
        return user.profile


@permission_classes((IsOwnerOrReadOnly,))
class ChangeSocial(UpdateAPIView):
    """
    Modifica las URL de los social network
    {
        "facebook": "url",
        "twitter": "url",
        "pinterest": "url",
        "dribbble": "url"
    }
    """
    queryset = Profile.objects.all()
    serializer_class = ChangeSocialSerializer
    lookup_field = 'username'

    def get_object(self):
        self.queryset = User.objects.all()
        user = super(ChangeSocial, self).get_object()
        return user.profile
